﻿using System.Net.Http.Json;
using Newtonsoft.Json;

namespace Backbone.Client.Application.Http
{
    internal static class HttpHelper
    {
        public static async Task<T> GetAsync<T>(string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, new Uri(url));
            return await RequestAsync<T>(request);
        }

        public static async Task<T> PostAsync<T>(string url, object body)
        {
            var request = new HttpRequestMessage(HttpMethod.Post, new Uri(url))
            {
                Content = JsonContent.Create(body),
            };

            return await RequestAsync<T>(request);
        }

        public static async Task PostAsync(string url, object body)
        {
            var request = new HttpRequestMessage(HttpMethod.Post, new Uri(url))
            {
                Content = JsonContent.Create(body),
            };

            await ProcessRequestAsync(request);
        }

        public static async Task PutAsync(string url, object body)
        {
            var request = new HttpRequestMessage(HttpMethod.Put, new Uri(url))
            {
                Content = JsonContent.Create(body),
            };

            await ProcessRequestAsync(request);
        }

        public static async Task DeleteAsync(string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Delete, new Uri(url));
            await ProcessRequestAsync(request);
        }

        private static async Task<T> RequestAsync<T>(HttpRequestMessage request)
        {
            using var client = new HttpClient();
            var response = await client.SendAsync(request);

            var responseStr = await response.Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<T>(responseStr);
            if (result == null)
                throw new NullReferenceException(nameof(result));

            return result;
        }

        private static async Task ProcessRequestAsync(HttpRequestMessage request)
        {
            using var client = new HttpClient();
            await client.SendAsync(request);
        }

    }
}
