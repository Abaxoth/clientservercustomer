﻿using Backbone.Client.Application.Http.Abstractions;

namespace Backbone.Client.Application.Http
{
    internal class AppHttpClient : IHttpClient
    {
        public AppHttpClient(ICustomerHttp customerHttp)
        {
            Customer = customerHttp;
        }

        public ICustomerHttp Customer { get; }
    }
}
