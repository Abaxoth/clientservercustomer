﻿namespace Backbone.Client.Application.Http.Abstractions
{
    public interface IHttpClient
    {
        public ICustomerHttp Customer { get; }
    }
}
