﻿using Backbone.Server.Dto;

namespace Backbone.Client.Application.Http.Abstractions
{
    public interface ICustomerHttp
    {
        public Task UpdateCustomerAsync(CustomerBaseDto customerBase, int id);
        public Task AddCustomerAsync(CustomerBaseDto customerBase);
        public Task DeleteCustomerAsync(int id);

        public Task<List<CustomerDto>> GetCustomersAsync(CustomerSearchFilterDto filter);
    }
}
