﻿using Backbone.Client.Application.Abstractions;
using Backbone.Client.Application.Http.Abstractions;
using Backbone.Server.Dto;

namespace Backbone.Client.Application.Http
{
    internal class CustomerHttp : ICustomerHttp
    {
        private readonly string _serverApi;

        public CustomerHttp(IAppConfig appConfig)
        {
            _serverApi = appConfig.BackboneServerAPI;
        }

        public async Task UpdateCustomerAsync(CustomerBaseDto customerBase, int id)
        {
            var url = BuildUrl(_serverApi, $"Customer/{id}");
            await HttpHelper.PutAsync(url, customerBase);
        }

        public async Task AddCustomerAsync(CustomerBaseDto customerBase)
        {
            var url = BuildUrl(_serverApi, $"Customer");
            await HttpHelper.PostAsync(url, customerBase);
        }

        public async Task DeleteCustomerAsync(int id)
        {
            var url = BuildUrl(_serverApi, $"Customer/{id}");
            await HttpHelper.DeleteAsync(url);
        }

        public async Task<List<CustomerDto>> GetCustomersAsync(CustomerSearchFilterDto filter)
        {
            var url = BuildUrl(_serverApi, $"Customer/Search");
            return await HttpHelper.PostAsync<List<CustomerDto>>(url, filter);
        }

        private string BuildUrl(string host, string queue)
        {
            return $"{host}/{queue}";
        }
    }
}
