﻿using Backbone.Client.Application.Http;
using Backbone.Client.Application.Http.Abstractions;
using Microsoft.Extensions.DependencyInjection;

namespace Backbone.Client.Application
{
    public static class ServiceCollectionEx
    {
        public static IServiceCollection AddClientApplication(this IServiceCollection serviceCollection)
        {
            return serviceCollection
                .AddHttp();
        }

        private static IServiceCollection AddHttp(this IServiceCollection serviceCollection)
        {
            return serviceCollection
                .AddScoped<ICustomerHttp, CustomerHttp>()
                .AddScoped<IHttpClient, AppHttpClient>();
        }
    }
}
