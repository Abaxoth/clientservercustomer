﻿namespace Backbone.Client.Application.Abstractions
{
    public interface IAppConfig
    {
        public string BackboneServerAPI { get; }

    }
}
