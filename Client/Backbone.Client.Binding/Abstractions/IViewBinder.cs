﻿using System.Windows.Controls;

namespace Backbone.Client.Binding.Abstractions
{
    public interface IViewBinder
    {
        void Bind(ContentControl view);
    }

    public interface IViewBinder<in TView> : IViewBinder
        where TView : ContentControl
    {
        void Bind(TView view);
    }
}
