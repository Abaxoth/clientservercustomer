﻿using System;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Backbone.Client.Binding.Abstractions
{
    public interface IViewInitializer
    {
        Task ShowByViewModelAsync<TViewModel>(TViewModel viewModel, bool asDialog)
            where TViewModel : BaseViewModel;

        Task ShowByViewModelAsync<TViewModel>(bool asDialog)
            where TViewModel : BaseViewModel;

        Task<TView> InitializeViewAsync<TView>(BaseViewModel viewModel)
            where TView : ContentControl, new();

        Task<TViewModel> GetViewModelAsync<TViewModel>()
            where TViewModel : BaseViewModel;

        Task<object> InitializeViewAsync(Type viewType);

        Task<TView> InitializeViewAsync<TView>()
            where TView : ContentControl, new();
    }
}
