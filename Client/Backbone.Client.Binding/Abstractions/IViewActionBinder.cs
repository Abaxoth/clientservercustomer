﻿using System.Windows.Controls;

namespace Backbone.Client.Binding.Abstractions
{
    public interface IViewActionBinder
    {

    }

    public interface IViewActionBinder<in TAction>: IViewActionBinder
        where TAction: IActionUI
    {
        public void Bind(ContentControl control, TAction action);
    }
}
