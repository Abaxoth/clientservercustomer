﻿using Backbone.Client.Binding.Abstractions;
using System;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows;
using Backbone.Client.Binding.Extensions;
using Microsoft.Extensions.DependencyInjection;


namespace Backbone.Client.Binding
{
    public class ViewInitializer : IViewInitializer
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly Assembly _assembly;

        public ViewInitializer(IServiceProvider serviceProvider, Assembly assembly)
        {
            _serviceProvider = serviceProvider;
            _assembly = assembly;
        }

        public async Task ShowByViewModelAsync<TViewModel>([NotNull] TViewModel viewModel, bool asDialog)
            where TViewModel : BaseViewModel
        {
            var viewType = typeof(TViewModel).GetAttribute<ViewModelAttribute>().ViewType;
            var viewInstance = await InitializeViewAsync(viewType);
            var window = (viewInstance as Window).NotNull($"View type: {viewType} is not window");
            window!.DataContext = viewModel;

            if (asDialog)
                window.ShowDialog();

            else window.Show();
        }

        public async Task ShowByViewModelAsync<TViewModel>(bool asDialog)
            where TViewModel : BaseViewModel
        {
            var viewModel = _serviceProvider.GetService<TViewModel>();
            if (viewModel == null)
                throw new NullReferenceException(nameof(viewModel));

            await ShowByViewModelAsync(viewModel, asDialog);
        }

        public async Task<TView> InitializeViewAsync<TView>(BaseViewModel viewModel)
            where TView : ContentControl, new()
        {
            var view = await InitializeViewAsync<TView>();
            view.DataContext = viewModel;
            return view;
        }

        public Task<TViewModel> GetViewModelAsync<TViewModel>() where TViewModel : BaseViewModel
        {
            var model = _serviceProvider.GetService<TViewModel>()
                        ?? throw new NullReferenceException(typeof(TViewModel).Name);

            return Task.FromResult(model);
        }

        public async Task<object> InitializeViewAsync(Type viewType)
        {
            var viewModels = _assembly
                .GetTypes().Where(x => x.GetCustomAttribute<ViewModelAttribute>() != null);

            foreach (var viewModelType in viewModels)
            {
                var attribute = viewModelType.GetAttribute<ViewModelAttribute>();
                if (attribute.ViewType != viewType) continue;

                var context = _serviceProvider.GetService(viewModelType);
                context.NotNull($"Can't find service: {viewModelType}");
                await ((BaseViewModel)context!).InitializeAsync();

                var view = (ContentControl)ActivatorUtilities.CreateInstance(_serviceProvider, viewType);
                view = view.NotNull($"Can't create instance of type: {viewType}");
                view.DataContext = context;
                BindView(view, ((BaseViewModel)context!));
                return view;
            }

            throw new Exception($"Can't initialize view of type: {viewType}");
        }

        public async Task<TView> InitializeViewAsync<TView>()
            where TView : ContentControl, new()
        {
            var view = await InitializeViewAsync(typeof(TView));
            return (TView)view;
        }


        private void BindView(ContentControl control, BaseViewModel viewModel)
        {
            var actionsTypes = viewModel.GetType()
                .GetInterfaces().Where(x => x.IsAssignableTo(typeof(IActionUI)));

            foreach (var actionsType in actionsTypes)
            {
                var bindersTypes = _assembly.GetTypes()
                    .Where(x => x.GetInterfaces()
                        .Any(c => c.IsAssignableTo(typeof(IViewActionBinder))
                                             && c.GenericTypeArguments.Contains(actionsType))).ToList();

                foreach (var binderType in bindersTypes)
                {
                    var bindMethod = binderType
                        .GetMethods().First(x => x.GetParameters().Any(p => p.ParameterType == actionsType));
                    var instance = ActivatorUtilities.CreateInstance(_serviceProvider, binderType);
                    bindMethod.Invoke(instance, new object?[]{ control, viewModel});
                }


            }


        }

        private void BindView(ContentControl control, Type viewType)
        {
            var actionsTypes = viewType.GetInterfaces().Where(x => x.IsAssignableFrom(typeof(IActionUI)));

            foreach (var actionsType in actionsTypes)
            {
                var a = 1;
            }


            //var handler = _assembly.GetTypes()
            //    .Where(x => x.GetInterfaces().Contains(typeof(IViewActionBinder)))
            //    .Where(x=> x.GetInterfaces().Where(i=> i.IsGenericType))


            //var types = _assembly.GetTypes()
            //    .Where(x => x.GetInterfaces().Contains(typeof(IViewBinder)))
            //    .Where(x => x.GetInterfaces().Where(i => i.IsGenericType)
            //        .Any(y => y.GenericTypeArguments.Contains(viewType))).ToList();

            //if (types.Count > 1)
            //    throw new Exception($"Contains 2 or more {typeof(IViewBinder<>).Name} for view: {viewType.Name}");

            //if (types.Count == 0)
            //{
            //    //If no binder is found, no action is required
            //    return;
            //}

            //var interfaceType = types.First()
            //    .GetInterfaces()
            //    .Where(x => x.IsGenericType)
            //    .First(x => x.GenericTypeArguments.Contains(viewType));

            //var binderObj = _serviceProvider.GetService(interfaceType);
            //if (binderObj == null)
            //    throw new Exception($"Can't resolve binder service for view: {viewType.Name}");

            //var binder = (IViewBinder)binderObj;
            //binder.Bind(control);
        }
    }
}
