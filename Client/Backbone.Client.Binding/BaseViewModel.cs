﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;

namespace Backbone.Client.Binding
{
    public class BaseViewModel : DependencyObject
    {
        public virtual Task InitializeAsync()
        {
            return Task.CompletedTask;
        }
    }

    public class BaseViewModel<TParentViewModel> : BaseViewModel,
        INotifyPropertyChanged 
        where TParentViewModel : class
    {
        private readonly Dictionary<object, object> _propName2PropValue = new();
        public event PropertyChangedEventHandler? PropertyChanged;

        public BaseViewModel()
        {

        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string? propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        protected T GetPropValue<T>(string propertyName)
        {
            if (!_propName2PropValue.ContainsKey(propertyName))
                _propName2PropValue.Add(propertyName, default!);

            var value = _propName2PropValue[propertyName];
            if (value == null) 
                return default!;

            return (T)value;
        }

        protected void SetPropValue<T>(string propertyName, T value)
        {
            _propName2PropValue[propertyName] = value!;
            OnPropertyChanged(propertyName);
        }
    }
}
