﻿using System;
using System.Reflection;

namespace Backbone.Client.Binding.Extensions
{
    internal static class TypeEx
    {
        public static TObject NotNull<TObject>(this TObject obj, string exceptionMessage)
        {
            if (obj == null) throw new Exception(exceptionMessage);
            return obj;
        }

        [return: System.Diagnostics.CodeAnalysis.NotNull]
        public static TObject NotNull<TObject>(this TObject obj)
        {
            if (obj == null) throw new Exception($"Object can't be NULL");
            return obj;
        }

        public static TAttribute GetAttribute<TAttribute>(this Type type)
            where TAttribute : Attribute
        {
            var attribute = type.GetCustomAttribute<TAttribute>();
            if (attribute == null)
                throw new NullReferenceException($"Can't find attribute: {typeof(TAttribute)} in type: {type}");

            return attribute;
        }

        public static TAttribute? GetAttributeOrNull<TAttribute>(this Type type)
            where TAttribute : Attribute
        {
            var attribute = type.GetCustomAttribute<TAttribute>();
            return attribute;
        }
    }
}
