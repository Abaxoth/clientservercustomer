﻿using System;
using System.Windows.Controls;

namespace Backbone.Client.Binding
{
    [AttributeUsage(AttributeTargets.Class)]
    public class ViewModelAttribute : Attribute
    {
        public ViewModelAttribute(Type viewType)
        {
            if (viewType == typeof(ContentControl))
                throw new Exception($"{viewType.Name} is not control type");


            ViewType = viewType;
        }

        public Type ViewType { get; }
    }
}
