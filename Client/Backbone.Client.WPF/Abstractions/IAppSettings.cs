﻿namespace Backbone.Client.WPF.Abstractions
{
    internal interface IAppSettings
    {
        public string AppConfigPath { get; }
    }
}
