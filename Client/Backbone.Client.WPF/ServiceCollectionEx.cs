﻿using System.Linq;
using System.Reflection;
using Backbone.Client.Application.Abstractions;
using Backbone.Client.Binding;
using Backbone.Client.Binding.Abstractions;
using Backbone.Client.WPF.Abstractions;
using Backbone.Client.WPF.Services;
using Microsoft.Extensions.DependencyInjection;

namespace Backbone.Client.WPF
{
    internal static class ServiceCollectionEx
    {
        public static IServiceCollection AddServices(this IServiceCollection serviceCollection)
        {
            return serviceCollection
                .AddViewBinding()
                .AddSettings()
                .AddLogging();
        }

        private static IServiceCollection AddViewBinding(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddScoped<IViewInitializer>
                (serviceProvider => new ViewInitializer(serviceProvider, Assembly.GetExecutingAssembly()));

            //bind view models
            var viewModels = Assembly.GetExecutingAssembly()
                    .GetTypes().Where(x => x.GetCustomAttribute<ViewModelAttribute>() != null);

            foreach (var viewModel in viewModels)
                serviceCollection.AddScoped(viewModel);

            return serviceCollection;
        }

        private static IServiceCollection AddSettings(this IServiceCollection serviceCollection)
        {
            return serviceCollection
            .AddScoped<IAppConfig, AppConfig>()
            .AddScoped<IAppSettings, AppSettings>();
        }

    }
}
