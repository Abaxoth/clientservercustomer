﻿using System.IO;
using Backbone.Client.WPF.Abstractions;

namespace Backbone.Client.WPF.Services
{
    internal class AppSettings : IAppSettings
    {
        public string AppConfigPath => Path.Combine("Data", "AppConfig.json");
    }
}
