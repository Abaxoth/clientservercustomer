﻿using System.IO;
using Backbone.Client.Application.Abstractions;
using Backbone.Client.WPF.Abstractions;
using Newtonsoft.Json.Linq;

namespace Backbone.Client.WPF.Services
{
    internal class AppConfig : IAppConfig
    {
        private readonly JObject _configJson;

        public AppConfig(IAppSettings appSettings)
        {
            AppSettings = appSettings;
            _configJson = JObject.Parse(File.ReadAllText(appSettings.AppConfigPath));
        }

        protected IAppSettings AppSettings { get; }

        public string BackboneServerAPI => _configJson["BackboneServerAPI"]?.Value<string>()!;
    }
}
