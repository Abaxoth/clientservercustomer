﻿using System.Windows;
using System.Windows.Controls;
using Backbone.Client.Binding.Abstractions;
using Backbone.Client.WPF.EventsUI.Abstractions;

namespace Backbone.Client.WPF.EventsUI.Handlers
{
    public class CloseViewBinder : IViewActionBinder<ICloseAction>
    {
        public void Bind(ContentControl control, ICloseAction action)
        {
            if (control is Window window)
            {
                action.CloseAction += () => { window.Close(); };
            }
        }
    }
}
