﻿using System;
using Backbone.Client.Binding.Abstractions;

namespace Backbone.Client.WPF.EventsUI.Abstractions
{
    public interface ICloseAction: IActionUI
    {
        event Action CloseAction;
    }
}
