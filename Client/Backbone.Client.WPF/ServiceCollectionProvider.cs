﻿using Microsoft.Extensions.DependencyInjection;
using Backbone.Client.Application;

namespace Backbone.Client.WPF
{
    internal class ServiceCollectionProvider
    {
        public IServiceCollection Get()
        {
            var serviceCollection = new ServiceCollection();
            serviceCollection.AddServices();
            serviceCollection.AddClientApplication();
            return serviceCollection;
        }
    }
}
