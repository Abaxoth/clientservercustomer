﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Backbone.Client.Application.Http.Abstractions;
using Backbone.Client.Binding;
using Backbone.Client.Binding.Abstractions;
using Backbone.Core.Models;
using Backbone.Server.Dto;
using DevExpress.Mvvm;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace Backbone.Client.WPF.UI
{

    [ViewModel(typeof(CustomerView))]
    internal class CustomerViewModel : BaseViewModel<CustomerViewModel>
    {
        public enum CustomerProperty
        {
            Name,
            [Description("Company name")]
            CompanyName,
            Email,
            Phone,
        }

        private const int ItemsPerPage = 10;
        private CustomerProperty? _selectedSearchProperty;
        private CustomerProperty? _selectedSortProperty;
        private SortType? _sortType;

        public CustomerViewModel()
        {
        }

        [ActivatorUtilitiesConstructor]
        public CustomerViewModel(ILogger<CustomerViewModel> logger, IHttpClient httpClient, IViewInitializer viewInitializer)
        {
            Logger = logger;
            HttpClient = httpClient;
            ViewInitializer = viewInitializer;
            IsEnabled = true;
        }

        protected ILogger Logger { get; }
        protected IHttpClient HttpClient { get; }
        protected IViewInitializer ViewInitializer { get; }

        public CustomerProperty[] FilterParams => Enum.GetValues(typeof(CustomerProperty)).Cast<CustomerProperty>().ToArray();

        public int CurrentPage
        {
            get => GetPropValue<int>(nameof(CurrentPage));
            set => SetPropValue(nameof(CurrentPage), value);
        }

        public bool IsEnabled
        {
            get => GetPropValue<bool>(nameof(IsEnabled));
            set => SetPropValue(nameof(IsEnabled), value);
        }

        public string FilterText
        {
            get => GetPropValue<string>(nameof(FilterText));
            set => SetPropValue(nameof(FilterText), value);
        }


        public ObservableCollection<CustomerDto> Customers { get; set; } = new();

        public ICommand SearchCommandAsync => new AsyncCommand(async () =>
        {
            await UpdateViewAsync();

        }, () => IsEnabled);

        public ICommand GoToNextPageCommandAsync => new AsyncCommand(async () =>
        {
            CurrentPage++;
            await UpdateViewAsync();

        }, () => IsEnabled && Customers.Count == ItemsPerPage);

        public ICommand GoToPreviousPageCommandAsync => new AsyncCommand(async () =>
        {
            if (CurrentPage == 0) return;
            CurrentPage--;
            await UpdateViewAsync();

        }, () => IsEnabled && CurrentPage > 0);

        public ICommand SortByNameAsync => new AsyncCommand(async () =>
        {
            _selectedSortProperty = CustomerProperty.Name;
            SwitchSortType();
            await UpdateViewAsync();

        }, () => IsEnabled);

        public ICommand SortByCompanyNameAsync => new AsyncCommand(async () =>
        {
            _selectedSortProperty = CustomerProperty.CompanyName;
            SwitchSortType();
            await UpdateViewAsync();

        }, () => IsEnabled);

        public ICommand SortByEmailAsync => new AsyncCommand(async  () =>
        {
            _selectedSortProperty = CustomerProperty.Email;
            SwitchSortType();
            await UpdateViewAsync();
        }, () => IsEnabled);

        public ICommand SortByPhoneAsync => new AsyncCommand(async () =>
        {
            _selectedSortProperty = CustomerProperty.Phone;
            SwitchSortType();
            await UpdateViewAsync();

        }, () => IsEnabled);

        public ICommand InitializeCommandAsync => new AsyncCommand(async () =>
        {
            await UpdateViewAsync();
        });

        public ICommand EditCustomerCommandAsync => new AsyncCommand<CustomerDto>(async (x) =>
        {
            var vm = await ViewInitializer.GetViewModelAsync<EditCustomerViewModel>();

            vm.Id = x.Id;
            vm.Name = x.Name;
            vm.CompanyName = x.CompanyName;
            vm.PhoneNumber = x.Phone;
            vm.Email = x.Email;

            await ViewInitializer.ShowByViewModelAsync(vm, true);
            var customer = vm.GetCustomerOrNull();
            if (customer == null) return;

            await HttpClient.Customer.UpdateCustomerAsync(customer, x.Id);
            await UpdateViewAsync();
        }, (x) => IsEnabled);

        public ICommand DeleteCustomerCommandAsync => new AsyncCommand<CustomerDto>(async (x) =>
        {
            await HttpClient.Customer.DeleteCustomerAsync(x.Id);
            await UpdateViewAsync();
        }, (x) => IsEnabled);

        public ICommand AddNewCustomerCommandAsync => new AsyncCommand(async () =>
        {
            var vm = await ViewInitializer.GetViewModelAsync<EditCustomerViewModel>();
            vm.Reset();
            await ViewInitializer.ShowByViewModelAsync(vm, true);
            var customer = vm.GetCustomerOrNull();
            if (customer == null) return;

            await HttpClient.Customer.AddCustomerAsync(customer);
            await UpdateViewAsync();
        }, ()=> IsEnabled);

        public ICommand SetFilterPropertyCommandAsync => new DelegateCommand<CustomerProperty>((x) =>
        {
            _selectedSearchProperty = x;
        });

        private async Task UpdateViewAsync()
        {
            try
            {
                IsEnabled = false;
                var filter = GetCustomerFilter();
                var items = await HttpClient.Customer.GetCustomersAsync(filter);
                Customers = new ObservableCollection<CustomerDto>(items);
                OnPropertyChanged(nameof(Customers));
            }
            finally
            {
                IsEnabled = true;
            }
        }

        private CustomerSearchFilterDto GetCustomerFilter()
        {
            var filter = new CustomerSearchFilterDto()
            {
                FromPos = CurrentPage * ItemsPerPage,
                ToPos = (CurrentPage + 1) * ItemsPerPage,
            };

            switch (_selectedSortProperty)
            {
                case CustomerProperty.Name:
                    filter.ByNameSort = _sortType;
                    break;
                case CustomerProperty.CompanyName:
                    filter.ByCompanyNameSort = _sortType;
                    break;
                case CustomerProperty.Email:
                    filter.ByEmailSort = _sortType;
                    break;
                case CustomerProperty.Phone:
                    filter.ByPhoneSort = _sortType;
                    break;
            }

            switch (_selectedSearchProperty)
            {
                case CustomerProperty.Name:
                    filter.NameStartWith = FilterText;
                    break;
                case CustomerProperty.CompanyName:
                    filter.CompanyNameStartWith = FilterText;
                    break;
                case CustomerProperty.Email:
                    filter.EmailStartWith = FilterText;
                    break;
                case CustomerProperty.Phone:
                    filter.PhoneStartWith = FilterText;
                    break;
            }

            return filter;
        }

        private void SwitchSortType()
        {
            switch (_sortType)
            {
                case null:
                case SortType.Ascending:
                    _sortType =  SortType.Descending;
                    break;
                case SortType.Descending:
                    _sortType = SortType.Ascending;
                    break;
                default:
                    throw new NotSupportedException();
            }
        }
    }
}
