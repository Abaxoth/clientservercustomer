﻿
using System;
using System.ComponentModel;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Input;
using Backbone.Client.Binding;
using DevExpress.Mvvm;
using Backbone.Client.WPF.EventsUI.Abstractions;
using Backbone.Server.Dto;
using Microsoft.Extensions.DependencyInjection;
using System.Net.Mail;
using System.Threading.Tasks;
using Backbone.Client.Application.Http.Abstractions;

namespace Backbone.Client.WPF.UI
{
    [ViewModel(typeof(EditCustomerView))]
    internal class EditCustomerViewModel : BaseViewModel<EditCustomerViewModel>, ICloseAction, IDataErrorInfo
    {
        private readonly string _phoneRegex = "^\\+?[1-9][0-9]{7,14}$";
        private bool _isSubmit;

        public EditCustomerViewModel(){}

        [ActivatorUtilitiesConstructor]
        public EditCustomerViewModel(IHttpClient httpClient)
        {
            HttpClient = httpClient;
            Reset();
        }

        protected IHttpClient HttpClient { get; }

        public int Id { get; set; }

        public string Name { get; set; } = string.Empty;
        public string CompanyName { get; set; } = string.Empty;
        public string Email { get; set; } = string.Empty;
        public string PhoneNumber { get; set; } = string.Empty;

        public string Error
        {
            get => GetPropValue<string>(nameof(Error));
            set => SetPropValue(nameof(Error), value);
        }
        public string this[string columnName]
        {
            get
            {
                string error = string.Empty;
                switch (columnName)
                {
                    case nameof(Name):
                        if (Name.Length < 3)
                            error = "Min length is: 3";
                        if (Name.Length > 64)
                            error = "Max length is: 64";
                        if (string.IsNullOrWhiteSpace(Name))
                            error = "Is Empty";
                        break;

                    case nameof(CompanyName):
                        if (CompanyName.Length < 3)
                            error = "Min length is: 3";
                        if (CompanyName.Length > 64)
                            error = "Max length is: 64";
                        if (string.IsNullOrWhiteSpace(CompanyName))
                            error = "Is Empty";
                        break;

                    case nameof(Email):
                        if (!IsValidEmail(Email))
                            error = "Wrong email format";
                        break;

                    case nameof(PhoneNumber):
                        if (!Regex.IsMatch(PhoneNumber, _phoneRegex))
                            error = "Wrong phone format";
                        break;

                }

                return error;
            }
        }

        public event Action CloseAction;

        public ICommand SubmitAsync => new AsyncCommand(async () =>
        {
            if (await IsCompanyAlreadyExistsAsync())
            {
                Error = $"Company: {CompanyName} already exists";
                return;
            }

            _isSubmit = true;
            CloseAction?.Invoke();

        }, IsValidData);

        public CustomerBaseDto? GetCustomerOrNull()
        {
            if (!_isSubmit || !IsValidData()) return null;

            var model = new CustomerBaseDto()
            {
                Name = Name,
                CompanyName = CompanyName,
                Email = Email,
                Phone = PhoneNumber,
            };

            return model;
        }

        public void Reset()
        {
            Name = string.Empty;
            CompanyName = string.Empty;
            Email = string.Empty;
            PhoneNumber = string.Empty;
            _isSubmit = false;
            Error = string.Empty;
            Id = 0;
        }

        private bool IsValidEmail(string email)
        {
            return MailAddress.TryCreate(email, out _);
        }

        private bool IsValidData()
        {
            return
                string.IsNullOrWhiteSpace(this[nameof(Name)]) &&
                string.IsNullOrWhiteSpace(this[nameof(CompanyName)]) &&
                string.IsNullOrWhiteSpace(this[nameof(Email)]) &&
                string.IsNullOrWhiteSpace(this[nameof(PhoneNumber)]);
        }

        private async Task<bool> IsCompanyAlreadyExistsAsync()
        {
            var filter = new CustomerSearchFilterDto()
            {
                CompanyName = CompanyName,
            };

            var anotherCustomers = await HttpClient.Customer.GetCustomersAsync(filter);

            //remove itself
            anotherCustomers = anotherCustomers.Where(x => x.Id != Id).ToList();
            return anotherCustomers.Any();
        }
    }
}
