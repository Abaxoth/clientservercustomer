﻿using Backbone.Client.Binding.Abstractions;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Windows;
using Backbone.Client.WPF.UI;

namespace Backbone.Client.WPF
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : System.Windows.Application
    {
        private readonly IServiceProvider _serviceProvider;

        public App()
        {
            _serviceProvider = GetServiceProvider();
        }

        private async void AppStartup(object sender, StartupEventArgs e)
        {
            var viewInitializer = _serviceProvider.GetService<IViewInitializer>();
            if (viewInitializer == null)
                throw new NullReferenceException(nameof(viewInitializer));

            var viewModel = await viewInitializer.GetViewModelAsync<CustomerViewModel>();
            await viewInitializer.ShowByViewModelAsync(viewModel, true);
        }

        private IServiceProvider GetServiceProvider()
        {
            var serviceCollection = new ServiceCollectionProvider().Get();
            return serviceCollection.BuildServiceProvider();
        }
    }
}
