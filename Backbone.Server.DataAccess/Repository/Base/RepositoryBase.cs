﻿using Backbone.Server.DataAccess.Repository.Abstractions;
using Backbone.Server.Domain.Abstractions;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace Backbone.Server.DataAccess.Repository.Base
{
    public class RepositoryBase<TEntity, TContext> : IRepository<TEntity>
        where TEntity : class, IEntity, new()
        where TContext : DbContext
    {
        private DbContext _context { get; }

        protected RepositoryBase(TContext context)
        {
            _context = context;
        }

        protected DbSet<TEntity> DbSet => _context.Set<TEntity>();
        public IQueryable<TEntity> Collection => DbSet;

        public virtual List<TEntity> GetAll(Expression<Func<TEntity, bool>> filter = null)
        {
            return
                filter == null
                    ? _context.Set<TEntity>().ToList()
                    : _context.Set<TEntity>().Where(filter).ToList();
        }


        public virtual TEntity Get(Expression<Func<TEntity, bool>> filter)
        {
            return _context.Set<TEntity>().SingleOrDefault(filter);
        }

        public void Add(TEntity entity)
        {
            var addedEntity = _context.Entry(entity);
            addedEntity.State = EntityState.Added;
            _context.SaveChanges();
        }

        public void Update(TEntity entity)
        {
            var updatedEntity = _context.Entry(entity);
            updatedEntity.State = EntityState.Modified;
            _context.SaveChanges();
        }

        public void Delete(TEntity entity)
        {
            var deletedEntity = _context.Entry(entity);
            deletedEntity.State = EntityState.Deleted;
            _context.SaveChanges();
        }
    }
}
