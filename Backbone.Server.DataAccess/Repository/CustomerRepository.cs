﻿using Backbone.Server.DataAccess.Repository.Abstractions;
using Backbone.Server.DataAccess.Repository.Base;
using Backbone.Server.Domain;

namespace Backbone.Server.DataAccess.Repository
{
    internal class CustomerRepository: RepositoryBase<Customer, AppDbContext>,
        ICustomerRepository
    {

        public CustomerRepository(AppDbContext context) : base(context)
        {
        }

        public void DeleteById(int id)
        {
            var customer = Get(x => x.Id == id);
            Delete(customer);
        }

        public void Do()
        {
            DbSet
                .Where(x => x.Email == "").Take(new Range(10, 19));
        }
    }
}
