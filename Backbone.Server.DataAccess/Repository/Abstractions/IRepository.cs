﻿using System.Linq.Expressions;
using Backbone.Server.Domain.Abstractions;

namespace Backbone.Server.DataAccess.Repository.Abstractions
{
    public interface IRepository<T> where T : class, IEntity, new()
    {
        public IQueryable<T> Collection { get; }

        List<T> GetAll(Expression<Func<T, bool>> filter = null);

        T Get(Expression<Func<T, bool>> filter);

        void Add(T entity);

        void Update(T entity);

        void Delete(T entity);
    }
}
