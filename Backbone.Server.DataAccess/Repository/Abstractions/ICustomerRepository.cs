﻿using Backbone.Server.Domain;

namespace Backbone.Server.DataAccess.Repository.Abstractions
{
    public interface ICustomerRepository : IRepository<Customer>
    {
        public void DeleteById(int id);
    }
}
