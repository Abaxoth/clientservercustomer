﻿using Microsoft.Extensions.DependencyInjection;
using Backbone.Server.DataAccess.Repository;
using Backbone.Server.DataAccess.Repository.Abstractions;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace Backbone.Server.DataAccess
{
    public static class ServiceCollectionEx
    {
        public static IServiceCollection AddDataAccessServices(this IServiceCollection services)
        {
            return services
                .AddScoped<ICustomerRepository, CustomerRepository>();
        }

        public static IServiceCollection AddSqlDatabase(
            this IServiceCollection services,
            Action<DbOptions> configureOptions)
        {
            if (services == null)
                throw new ArgumentNullException(nameof(services));

            if (configureOptions == null)
                throw new ArgumentNullException(nameof(configureOptions));

            return services
                .Configure(configureOptions)
                .AddDbContext<AppDbContext>((serviceProvider, dbOptions) =>
                {
                    var options = serviceProvider.GetService<IOptions<DbOptions>>()?.Value;

                    if (string.IsNullOrWhiteSpace(options?.ConnectionString))
                        throw new InvalidOperationException("Connection string is empty");

                    dbOptions.UseNpgsql(options.ConnectionString);
                });
        }
    }

   
}