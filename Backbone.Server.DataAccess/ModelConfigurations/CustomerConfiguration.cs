﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using Backbone.Server.Domain;
using Microsoft.EntityFrameworkCore.Infrastructure;

namespace Backbone.Server.DataAccess.ModelConfigurations
{
    internal class CustomerConfiguration : IEntityTypeConfiguration<Customer>
    {
        public void Configure(EntityTypeBuilder<Customer> builder)
        {
            builder.ToTable("Customers");
            builder.HasIndex(x => x.CompanyName).IsUnique();
        }
    }
}
