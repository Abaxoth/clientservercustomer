﻿using Microsoft.Extensions.Options;

namespace Backbone.Server.DataAccess
{
    public class DbOptions : IOptions<DbOptions>
    {
        public string ConnectionString { get; set; }

        DbOptions IOptions<DbOptions>.Value => this;
    }
}
