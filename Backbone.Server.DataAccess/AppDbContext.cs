﻿using Microsoft.EntityFrameworkCore;
using Backbone.Server.Domain;

namespace Backbone.Server.DataAccess
{
    public class AppDbContext : DbContext
    {
        public virtual DbSet<Customer> Customers { get; set; }

        public AppDbContext(DbContextOptions<AppDbContext> options)
            : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            //Apply configuration from ModelConfigurations folder
            modelBuilder.ApplyConfigurationsFromAssembly(GetType().Assembly);
        }
    }
}
