﻿using Backbone.Server.Core.Validation;
using Backbone.Server.Core.Validation.Abstractions;
using Microsoft.Extensions.DependencyInjection;

namespace Backbone.Server.Core
{
    public static class ServiceCollectionEx
    {
        public static IServiceCollection AddCoreServices(this IServiceCollection services)
        {
            return services
                .AddValidation();
        }

        private static IServiceCollection AddValidation(this IServiceCollection services)
        {
            return services
                .AddScoped<IValidationService, ValidationService>();
        }
    }
}
