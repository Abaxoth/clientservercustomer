﻿using System.Reflection;
using Backbone.Server.Core.Validation.Abstractions;
using FluentValidation;

namespace Backbone.Server.Core.Validation
{
    public class ValidationService : IValidationService
    {
        public void Validate<T>(T obj)
        {
            if (obj == null)
                throw new ArgumentNullException();

            var abstractValidatorType = typeof(AbstractValidator<>);
            var objectType = obj.GetType();
            var genericType = abstractValidatorType.MakeGenericType(objectType);

            var validatorType = FindValidatorType(objectType.Assembly, genericType);
            if (validatorType == null) return;

            var validatorInstance = (IValidator)Activator.CreateInstance(validatorType);
            var context = new ValidationContext<T>(obj);

            //You can check an errors in the validationResult.Errors 
            var validationResult = validatorInstance?.Validate(context);

            if (validationResult is { IsValid: false })
                throw new ValidationException("Validation failed");
        }

        private static Type FindValidatorType(Assembly assembly, Type evt)
        {
            if (assembly == null)
                throw new ArgumentNullException(nameof(assembly));

            if (evt == null)
                throw new ArgumentNullException(nameof(evt));

            return assembly.GetTypes().FirstOrDefault(t => t.IsSubclassOf(evt));
        }
    }
}
