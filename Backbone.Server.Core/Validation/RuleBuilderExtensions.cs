﻿using FluentValidation;

namespace Backbone.Server.Core.Validation
{
    public static class RuleBuilderExtensions
    {
        public static IRuleBuilderOptions<T, string> IsNotEmptyAndWhiteSpace<T>(this IRuleBuilder<T, string> ruleBuilder)
        {
            return ruleBuilder.Must((root, value, context) =>
            {
                if (value == null)
                    return true;

                return !string.IsNullOrWhiteSpace(value);
            }).WithErrorCode("IsNotEmptyAndWhiteSpaceValidator");
        }
    }
}
