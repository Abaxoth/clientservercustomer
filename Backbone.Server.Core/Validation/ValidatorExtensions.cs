﻿using FluentValidation;

namespace Backbone.Server.Core.Validation
{
    public static class ValidatorExtensions
    {
        public static IRuleBuilderOptions<T, string> IsName<T>(this IRuleBuilder<T, string> ruleBuilder)
        {
            return ruleBuilder.RuleForNames(3, 64);
        }

        public static IRuleBuilderOptions<T, string> IsPhone<T>(this IRuleBuilder<T, string> ruleBuilder)
        {
            return ruleBuilder.Matches("^\\+?[1-9][0-9]{7,14}$");
        }

        private static IRuleBuilderOptions<T, string> RuleForNames<T>(
            this IRuleBuilder<T, string> ruleBuilder,
            int minLength, int maxLength)
        {
            return ruleBuilder
                .NotNull()
                .IsNotEmptyAndWhiteSpace()
                .MaximumLength(maxLength)
                .MinimumLength(minLength);
        }
    }
}
