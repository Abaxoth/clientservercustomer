﻿namespace Backbone.Server.Core.Validation.Abstractions
{
    public interface IValidationService
    {
        void Validate<T>(T obj);
    }
}
