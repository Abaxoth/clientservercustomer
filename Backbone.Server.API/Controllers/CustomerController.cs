using Backbone.Server.Application.Services.Abstractions;
using Backbone.Server.Domain;
using Backbone.Server.Dto;
using Microsoft.AspNetCore.Mvc;

namespace Backbone.Server.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CustomerController : ControllerBase
    {

        public CustomerController(ILogger<CustomerController> logger, ICustomerService customerService)
        {
            Logger = logger;
            CustomerService = customerService;
        }

        protected ILogger Logger { get; }
        protected ICustomerService CustomerService { get; }

        [HttpPost]
        public async Task Add(CustomerBaseDto customerBase)
        {
            await CustomerService.AddAsync(customerBase);
        }

        [HttpGet("{id:int}")]
        public async Task<Customer> GetById(int id)
        {
            var item = await CustomerService.GetByIdAsync(id);
            return item;
        }

        [HttpPut("{id:int}")]
        public async Task Update(CustomerBaseDto customerBase, int id)
        {
            await CustomerService.UpdateAsync(customerBase, id);
        }

        [HttpDelete("{id:int}")]
        public async Task Delete(int id)
        {
            await CustomerService.DeleteAsync(id);
        }
        
        [HttpPost("Search")]
        public async Task<List<Customer>> SearchByFilter(CustomerSearchFilterDto filter)
        {
            return await CustomerService.GetByFilter(filter);
        }


    }
}