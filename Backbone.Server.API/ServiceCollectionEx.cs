﻿using Backbone.Server.Application;
using Backbone.Server.Core;
using Backbone.Server.DataAccess;

namespace Backbone.Server.API
{
    public static class ServiceCollectionEx
    {

        public static IServiceCollection AddModules(this IServiceCollection services)
        {
            return services
                .AddCoreServices()
                .AddApplicationServices()
                .AddDataAccessServices();


        }

        public static IServiceCollection AddDatabase(this IServiceCollection services)
        {
            var connectionString = "Host=localhost;Username=postgres;Password=111;Database=Backbone";

            return services.AddSqlDatabase(options =>
             {
                 options.ConnectionString = connectionString;
             });
        }
    }
}
