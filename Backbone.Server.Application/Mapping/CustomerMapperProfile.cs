﻿using AutoMapper;
using Backbone.Server.Domain;
using Backbone.Server.Dto;

namespace Backbone.Server.Application.Mapping
{
    public class CustomerMapperProfile: Profile
    {
        public CustomerMapperProfile()
        {
            CreateMap<CustomerBaseDto, Customer>().ReverseMap();
            CreateMap<CustomerDto, Customer>().ReverseMap();
            CreateMap<CustomerBaseDto, CustomerDto>().ReverseMap();
        }
    }
}
