﻿using Backbone.Server.Application.Services;
using Backbone.Server.Application.Services.Abstractions;
using Microsoft.Extensions.DependencyInjection;

namespace Backbone.Server.Application
{
    public static class ServiceCollectionEx
    {
        public static IServiceCollection AddApplicationServices(this IServiceCollection services)
        {
            return services
                .AddScoped<ICustomerService, CustomerService>()
                .AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

        }

    }
}
