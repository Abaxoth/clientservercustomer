﻿using Backbone.Server.Domain;
using Backbone.Server.Dto;

namespace Backbone.Server.Application.Services.Abstractions
{
    public interface ICustomerService
    {
        Task AddAsync(CustomerBaseDto customerBase);
        Task<List<Customer>> GetAllAsync();
        Task<Customer> GetByIdAsync(int id);
        Task UpdateAsync(CustomerBaseDto updateData, int id);
        Task DeleteAsync(int id);
        Task<List<Customer>> GetByFilter(CustomerSearchFilterDto filter);
    }
}
