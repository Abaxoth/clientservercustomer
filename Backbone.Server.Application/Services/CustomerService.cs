﻿using AutoMapper;
using Backbone.Core.Models;
using Backbone.Server.Application.Services.Abstractions;
using Backbone.Server.Core.Validation.Abstractions;
using Backbone.Server.DataAccess.Repository.Abstractions;
using Backbone.Server.Domain;
using Backbone.Server.Dto;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Backbone.Server.Application.Services
{
    internal class CustomerService : ICustomerService
    {
        public CustomerService(ILogger<CustomerService> logger,
            ICustomerRepository customerRepository, IMapper mapper, IValidationService validationService)
        {
            Logger = logger;
            CustomerRepository = customerRepository;
            Mapper = mapper;
            ValidationService = validationService;
        }

        protected ILogger Logger { get; }
        protected ICustomerRepository CustomerRepository { get; }
        protected IMapper Mapper { get; }
        protected IValidationService ValidationService { get; }


        public Task AddAsync(CustomerBaseDto customerBase)
        {
            ValidationService.Validate(customerBase);
            var entity = Mapper.Map<Customer>(customerBase);
            CustomerRepository.Add(entity);
            Logger.LogInformation("Customer added");
            return Task.CompletedTask;
        }

        public Task<List<Customer>> GetAllAsync()
        {
            return Task.FromResult(CustomerRepository.GetAll());
        }

        public Task<Customer> GetByIdAsync(int id)
        {
            return Task.FromResult(CustomerRepository.Get(x => x.Id == id));
        }

        public Task UpdateAsync(CustomerBaseDto updateData, int id)
        {
            var entity = Mapper.Map<Customer>(updateData);
            var curEntity = CustomerRepository.Get(x => x.Id == id);

            curEntity.CompanyName = entity.CompanyName;
            curEntity.Email = entity.Email;
            curEntity.Phone = entity.Phone;
            curEntity.Name = entity.Name;

            CustomerRepository.Update(curEntity);
            return Task.CompletedTask;
        }

        public Task DeleteAsync(int id)
        {
            CustomerRepository.DeleteById(id);
            Logger.LogInformation($"User removed by id: {id}");
            return Task.CompletedTask;
        }

        public async Task<List<Customer>> GetByFilter(CustomerSearchFilterDto filter)
        {
            var query = CustomerRepository.Collection;

            if (filter.NameStartWith != null)
                query = query.Where(x => x.Name.StartsWith(filter.NameStartWith));

            if (filter.CompanyNameStartWith != null)
                query = query.Where(x => x.CompanyName.StartsWith(filter.CompanyNameStartWith));

            if (filter.EmailStartWith != null)
                query = query.Where(x => x.Email.StartsWith(filter.EmailStartWith));

            if (filter.PhoneStartWith != null)
                query = query.Where(x => x.Phone.StartsWith(filter.PhoneStartWith));

            switch (filter.ByNameSort)
            {
                case SortType.Ascending:
                    query = query.OrderBy(x => x.Name);
                    break;
                case SortType.Descending:
                    query = query.OrderByDescending(x => x.Name);
                    break;
            }

            switch (filter.ByCompanyNameSort)
            {
                case SortType.Ascending:
                    query = query.OrderBy(x => x.CompanyName);
                    break;
                case SortType.Descending:
                    query = query.OrderByDescending(x => x.CompanyName);
                    break;
            }

            switch (filter.ByEmailSort)
            {
                case SortType.Ascending:
                    query = query.OrderBy(x => x.Email);
                    break;
                case SortType.Descending:
                    query = query.OrderByDescending(x => x.Email);
                    break;
            }

            switch (filter.ByPhoneSort)
            {
                case SortType.Ascending:
                    query = query.OrderBy(x => x.Phone);
                    break;
                case SortType.Descending:
                    query = query.OrderByDescending(x => x.Phone);
                    break;
            }

            if (filter.ToPos != null && filter.FromPos != null)
            {
                query = query.Skip(filter.FromPos.Value).Take(filter.ToPos.Value - filter.FromPos.Value);
            }

            if (filter.CompanyName != null)
            {
                query = query.Where(x => x.CompanyName == filter.CompanyName);
            }

            var customers = await query.ToListAsync();
            return Mapper.Map<List<Customer>>(customers);
        }
    }
}
