﻿using Backbone.Server.Core.Validation;
using FluentValidation;

namespace Backbone.Server.Dto
{
    public class CustomerBaseDto
    {
        public string Name { get; set; }

        public string CompanyName { get; set; }

        public string Phone { get; set; }

        public string Email { get; set; }
    }

    public class CustomerBaseDtoValidator : AbstractValidator<CustomerBaseDto>
    {
        public CustomerBaseDtoValidator()
        {
            RuleFor(x => x.Name).IsName();
            RuleFor(x => x.CompanyName).IsName();
            RuleFor(x => x.Email).EmailAddress();
            RuleFor(x => x.Phone).IsPhone();
        }
    }
}