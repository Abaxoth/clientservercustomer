﻿using Backbone.Core.Models;

namespace Backbone.Server.Dto
{
    public class CustomerSearchFilterDto
    {
        public int? FromPos { get; set; }
        public int? ToPos { get; set; }

        public SortType? ByNameSort { get; set; }
        public SortType? ByPhoneSort { get; set; }
        public SortType? ByCompanyNameSort { get; set; }
        public SortType? ByEmailSort { get; set; }

        public string? NameStartWith { get; set; }
        public string? CompanyNameStartWith { get; set; }
        public string? EmailStartWith { get; set; }
        public string? PhoneStartWith { get; set; }

        public string? CompanyName { get; set; }
    }
}
