﻿namespace Backbone.Core.Models
{
    public enum SortType
    {
        Ascending,
        Descending,
    }
}
